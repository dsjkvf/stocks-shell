#!/bin/bash

# enter the workspace
cd $(dirname $0)

# detect the column with the tickers
tickerCol=$(cat $1 | grep TICKER | sed 's/label \(.\)[0-9].*/\1/')

# detect the column with the current price
priceCol=$(cat $1 | grep 'CURR PRICE' | sed 's/label \(.\)[0-9].*/\1/' | grep '^.$')

# detect the column with the current status
statusCol=$(cat $1 | grep STATUS | sed 's/label \(.\)[0-9].*/\1/')

# read the tickers
tickers=$(cat $1 | grep "label $tickerCol" | grep -v TICKER | sed 's/[^"]*"\([^"]*\)"/\1/')

# update the tickers
# start from the first row
cnt=1
for i in $tickers
do
    # if the row is not CLOSED
    if ! cat $1 | grep "label $statusCol$cnt" | grep -q 'CLOSED'
    then
        # read the data
        read -r -a marketPrice <<< $(curl -sk "https://query1.finance.yahoo.com/v7/finance/quote?lang=en-US&region=US&corsDomain=finance.yahoo.com&symbols=$i" | \
                                        jq '.quoteResponse.result|.[]|.regularMarketPrice,.preMarketPrice,.postMarketPrice' 2> /dev/null)
        # if the data was read correctly...
        if [ ${#marketPrice[@]} -eq 3 ]
        then
            # use preMarketPrice if available (and use regularMarketPrice otherwise)
            ! echo ${marketPrice[1]} | grep -q null && price=${marketPrice[1]} || price=${marketPrice[0]}
            # use postMarketPrice if available (and keep regularMarketPrice otherwise)
            ! echo ${marketPrice[2]} | grep -q null && price=${marketPrice[2]}
            # and if that price is indeed a number...
            if echo $price | awk '{ exit(1 - ($0 == $0 + 0)) }'
            then
                # update the sheet
                sed -i.bak "s/^let $priceCol$cnt =.*/let $priceCol$cnt = $price/" $1
            fi
        fi
    fi
    # move to the next row
    ((cnt++))
done

# open the sheet
scim $1

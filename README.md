# About

This is a collection of some simple shell scripts that aim to simplify obtaining some basic stocks information.

  - `price-btc,` `price-stocks` & `price-stocks-ticker` are supposed to run in the background and inform about swings in Bitcoin, predefined stocks and stocks from ticker's program configuration file (make sure to check before using);
  - `stock` provides with either just a stock's price, or, if requested via the `info` option, some other additional information;
  - `stock-wrapped` is a wrapper for `stock`;
  - `stocks-revolut-list` gets the list of Revolut supported stocks:
    - the primary source: https://globefunder.com/revolut-stocks-list/
    - the secondary source: https://github.com/nmapx/revolut-stocks-list/
  - `stocks-revolut-check` checks if a provided stock is present in a list of Revolut supported stocks;
  - `extras/scim/port.sh` is a sample script to update the predefined portfolio SC sheet;
  - `extras/scim/port_native.sc` is a sample portfolio SC sheet that doesn't require an external updater.
